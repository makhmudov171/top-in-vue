import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

import Home from "@/pages/Home";
import Search from "@/pages/Search";
import AddProduct from "@/pages/AddProduct";
import Profile from "@/pages/Profile";
import User from "@/pages/User";
import Prewiew from "@/pages/Prewiew";
import Entering1 from "@/pages/Entering1";
import Entering2 from "@/pages/Entering2";
import Entering3 from "@/pages/Entering3";
import Registration from "@/pages/Registration";
import Replenishment from "@/pages/Replenishment";
import Reclam from "@/pages/Reclam";
import MessageChat from "@/pages/MessageChat";
import Settings from "@/pages/Settings";
import ProfileReg from "@/pages/ProfileReg";
import Ads from "@/pages/Ads";
import Operations from "@/pages/Operations";
import Messages from "@/pages/Messages";
import Favorite from "@/pages/Favorite";
import Cart from "@/pages/Cart";

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
    },
    {
      path: "/search",
      name: "search",
      component: Search,
    },
    {
      path: "/addProduct",
      name: "addProduct",
      component: AddProduct,
    },
    {
      path: "/profile",
      name: "profile",
      component: Profile,
    },
    {
      path: "/user",
      name: "user",
      component: User,
    },
    {
      path: "/prewiew",
      name: "prewiew",
      component: Prewiew,
    },
    {
      path: "/entering1",
      name: "entering1",
      component: Entering1,
    },
    {
      path: "/entering2",
      name: "entering2",
      component: Entering2,
    },
    {
      path: "/entering3",
      name: "entering3",
      component: Entering3,
    },
    {
      path: "/registration",
      name: "registration",
      component: Registration,
    },
    {
      path: "/replenishment",
      name: "replenishment",
      component: Replenishment,
    },
    {
      path: "/reclam",
      name: "reclam",
      component: Reclam,
    },
    {
      path: "/messages/chat",
      name: "MessageChat",
      component: MessageChat,
    },
    {
      path: "/settings",
      name: "Settings",
      component: Settings,
    },
    {
      path: "/profileReg",
      name: "ProfileReg",
      component: ProfileReg,
    },
    {
      path: "/ads",
      name: "Ads",
      component: Ads,
    },
    {
      path: "/operations",
      name: "Operations",
      component: Operations,
    },
    {
      path: "/messages",
      name: "Messages",
      component: Messages,
    },
    {
      path: "/favorite",
      name: "Favorite",
      component: Favorite,
    },
    {
      path: "/cart",
      name: "Cart",
      component: Cart,
    },
  ],
});
