import Vue from 'vue'
import App from './App.vue'

import 'vue-slider-component/theme/default.css'
import "vue-select/dist/vue-select.css";

import router from './routes'
import VueSlider from 'vue-slider-component'
import vSelect from 'vue-select'
import '@/assets/scss/main.scss'

Vue.component('VueSlider', VueSlider, 'v-select', vSelect)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
